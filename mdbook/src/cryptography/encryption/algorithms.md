# Algorithms

## Symmetric-Key Algorithms

Here's some info about Symmetric-Key Algorithms:

| Name | Key Length | IV Length | Ciphertext Length<br/>(Multiples of) |
| --- | --- | --- | --|
| AES-128 | 16B | 16B | CBC/ECB: 16B <br/> Other: 2B |
| AES-256 | 24B | 16B | CBC/ECB: 16B <br/> Other: 2B |
| AES-512 | 32B | 16B | CBC/ECB: 16B <br/> Other: 2B |
| [Blowfish](https://en.wikipedia.org/wiki/Blowfish_(cipher)) | 4-56B | 8B | CBC/ECB: 8B <br/> Other: 2B |
| RC2 | Variable | Variable | 8B |
| RC4 | 1-256B | ? | ? |
| DES | 8B | 8B | 8B |
| 3DES | 24B | 8B | 8B |

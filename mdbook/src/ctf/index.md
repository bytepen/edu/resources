# Capture the Flag

Capture the Flag events are a great way to challenge yourself and sharpen your skills.

## Find Events
> [CTFTime](https://ctftime.org)
>> Website used to spread awareness of upcoming CTF Events.

## Ongoing

There are a handful of awesome websites out there that provide on-demand access to Capture the Flag challenges.

> [HackTheBox](https://hackthebox.eu)
>> Very well known website to practice your host-based hacking skills. New boxes coming online all the time and you can get a premium account to gain access to retired boxes and write-ups for those boxes.
>
> [TryHackMe](https://tryhackme.com)
>> Great website for practicing specific exploits that have come out recently.
>
> [Hacker101](https://hacker101.com)
>> Created by HackerOne to help people become familiar with web based pen testing.
>
> [OverTheWire](https://overthewire.org)
>> Most well known for their "Bandit" series of challenges, but they offer a lot more. Good website for learning Linux via CTF style challenges.
>
> [ringzer0](https://ringzer0ctf.com/challenges)
>> I haven't taken the time to mess around with this one, just yet, but it is a very well known CTF website with many good challenges.
>
> [CryptoHack](https://cryptohack.org)
>> Great website for learning Cryptography and how to abuse it with python.

## CTF Tips

> [CTF101](https://ctf101.org)
>> There are a handful of things that you can know about before going into a CTF which makes them a lot easier. This website contains many tips for doing CTFs.

## Write-Ups

It is common practice for some hackers to write guides on how to complete CTF challenges when allowed by the CTF organizers. Here is a list of hackers who enjoy doing such write-ups.

> [My Write-Ups](https://bytepen.gitlab.io/ctf)
>> I've been enjoying doing write-ups on CTF's I've been a part of when I can, here's where I post them.
>
> [IppSec - YouTube](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA)
>> IppSec is a very well known YouTuber who has many video write-ups on HackTheBox boxes.

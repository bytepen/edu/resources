# Training

Here's a list of training materials that don't fit neatly in another category.

> [CyberProfDevelopmentCovidResources](https://github.com/gerryguy311/CyberProfDevelopmentCovidResources/blob/master/README.md)
>> Great list of conferences, training, labs, books, podcasts, etc. that are free or otherwise available because of COVID-19.

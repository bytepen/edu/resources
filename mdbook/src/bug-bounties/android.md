## Basic Setup

> Description
> 
> It may not be exactly straight forward how you should set up your Android device for Pen Testing. Here are some resources to help you out.
> 
> Resources
>  
>  [Android 10 Device Setup](https://whiteoaksecurity.com/blog/2020/2/25/android-10-device-setup-for-penetration-testing)
>> Awesome guide on a few things to get you started while using an Android 10 device for Pen Testing.

## Certificate Pinning

> Description
>
> One very common flaw in Android Applications is certificate pinning. Essentially, if an application doesn't check for proper certificates, it can be trivial for the application to fall victim to an SSL MITM attack.
>
> Resources
>
> [Android Security SSL Pinning](https://medium.com/@appmattus/android-security-ssl-pinning-1db8acb6621e)
>> This is a good write-up on SSL Pinning and how to do it properly in Android Apps.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEyNTk4NTQ0MjMsMTMwMTM5NDExN119
-->
# Linux Operating System

## General
> [Linux From Scratch](http://www.linuxfromscratch.org/)
>> Linux From Scratch walks you through creation of a Linux OS. It is very interesting for understanding the minute details of Linux and is orchestrated in a very easy to navigate way.
>
> [You Don't Know Jack About .bash_history](https://www.youtube.com/watch?v=wv1xqOV2RyE)
>> Good SANS DFIR talk about how .bash_history works.

## Forensics
> [Hal Pomeranz Linux Forensics Intro](https://archive.org/details/HalLinuxForensics)
>> Very in depth presentation on Linux Forensics complete with hands-on exercises.

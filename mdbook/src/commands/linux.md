# Linux Commands

This page contains tools primarily used in the Linux terminal that are more complex and that I believe more people would benefit from really digging into.

## Awk

> **Description**
>
> Awk is a fantastic Linux command line utility that provides a ton of capabilities for parsing text. It goes far beyond the way most people use it in a Linux pipeline (Example: `cat /etc/shadow | awk -F: '/^root/{print $2}') and is actually a fully fledged scripting language.
>
> **Resources**
>
> [Awk Grymoire](https://www.grymoire.com/Unix/Awk.html)
>> The best in-depth guide to Awk I've ever seen.



# Commands

## General
> [Command Line Kung Fu](http://blog.commandlinekungfu.com/)
>> Great blog on some crazy stuff you can do in the Linux Command Line
>
> [ExplainShell](https://explainshell.com/)
>> Got some crazy one-liner you don't understand? This website tries to parse it for you.

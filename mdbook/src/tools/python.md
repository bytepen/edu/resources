# Python Tools

## Scapy
> **Description**
>
> Scapy is an awesome module that lets you mess with network packets
>
> **Resources**
>
> [Scapy ReadTheDocs](https://scapy.readthedocs.io/en/latest/introduction.html)
>> Good overview of all of the things you can do with Scapy.
>
> [The Art of Packet Crafting with Scapy](https://0xbharath.github.io/art-of-packet-crafting-with-scapy/)
>> Awesome, very in-depth, write-up on how to use Scapy to craft specific packets.


# Tools

I will be compiling a list of tools as I use them and find great value in them. They'll be split into several categories:

## [Mobile](/tools/mobile.md)
> Tools installed on mobile devices.

## [Online](/tools/online.md)
> Tools that are used primarily online, but may be able to be self-hosted.

## [Native](/tools/native.md)
> Tools installed directly on your computer.

## [Python](/tools/python.md)
> Libraries and Modules used within Python.

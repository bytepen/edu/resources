# Online Tools

This is a collection of tools that I use primarily online in alphabetical order. When possible, I will note when applications can be self hosted. This is nice because rather than reaching out to a website, sometimes you want to work locally or can't access the internet.

## ASCIIFlow Infinity
> **Description**
>
> Sometimes you just need a super quick diagram without a lot of complexities. This is a great tool for quickly keeping track of things like network diagrams and other things where a flow chart would come in handy.
>
> **Resources**
>
> [ASCIIFlow Infinity](http://asciiflow.com/)
>> Use the hosted version here.
> 
> [GitHub](https://github.com/lewish/asciiflow2)
>> ASCIIFlow Infinity is open source and can be self-hosted.

## CyberChef
> **Description**
>
> Described as "The Cyber Swiss Army Knife - a web app for encryption, encoding, compression and data analysis", CyberChef is a fantastic tool that allows you to convert between various formats and extract and analyze data.
>
> **Resources**
>
> [CyberChef](https://gchq.github.io/CyberChef)
>> Use the hosted version here.
>
> [GitHub](https://github.com/gchq/CyberChef)
>> CyberChef is open source and can be self-hosted.

## ExplainShell
> **Description**
>
> Ever come across a crazy Linux one-liner you can't figure out? Drop it in here and it will try to walk you through it.
>
> **Resources**
>
> [ExplainShell](https://explainshell.com)
>> Link to the ExplainShell Website.

## StackEdit
> **Description**
>
> Markdown is an incredibly useful tool for quickly taking notes or writing up documentation (like this website) without having to worry about HTML tags, CSS, and other formatting. StackEdit makes working with markdown files super easy and convenient. You can either upload your markdown files directly to StackEdit, or host them in a variety of locations, such as GitLab, GitHub, Google Drive, and Dropbox. They also have support for a handful of markdown plugins, such as mermaid which can be used for quickly creating charts and diagrams.
>
> **Resources**
> 
> [StackEdit](https://stackedit.io)
>> Use the hosted version here.
>
> [GitHub](https://github.com/benweet/stackedit)
> StackEdit is open source and can be self-hosted.

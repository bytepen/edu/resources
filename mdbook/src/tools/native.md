# Native Tools

This is a list of tools that are installed directly on your computer. This isn't mean to be a complete list of tools by any means, just a list of tools I use and think more people should have experience with. If you feel some awesome tools are missing, please let me know by submitting an [Issue on GitLab](https://gitlab.com/bytepen/education/hacker-resources/-/issues)!

## Android Studio
> **Description**
>
> Android Studio is primarily used to develop Android apps, but it also has a pretty great tool that can help you analyze .apk files. By default, it only shows you the Smali code, which can be a little intimidating, but if you have decompiled the apk with something like jadx, you can import the Java files to step through and debug the program with the aid of an Android emulator or actual device.

## Autopsy
> **Description**
>
> This is an amazing tool for analyzing disc images. After importing an image or Encase file, you can easily transverse and review everything. It even has automated scanners to help bring to light things you might otherwise miss and a full registry viewer. This is a must have if you're doing any sort of Forensic Analysis.
>
> **Platforms**
>
> * Linux
> * Windows
>
> **Resources**
>
> [Project Page](https://www.sleuthkit.org/autopsy/)
>> The Sleuth Kit's Page on their Autopsy software. Good overview of the project.
>
> [Installation Notes for Kali](https://github.com/sleuthkit/autopsy/issues/3845)
>> The version that can be found on Kali is from 2010 and is INCREDIBLY outdated. Its creators have been working on it since then and it has come a VERY long way. If you're prompted to use Autopsy in a browser, you're using the old version. I highly recommend that you dump it and install the latest version. This link has some notes on how to do so.

## Chisel
> **Description**
>
> Chisel is a nice tool that can be used to set up a reverse socks5 proxy to work around firewalls.
>
> **Platforms**
>
> * Linux
> * Windows
>
> **Resources**
>
> [GitHub](https://github.com/jpillora/chisel)
>> The Chisel Project on GitHub
>
> [Pivot with Chisel](https://www.puckiestyle.nl/pivot-with-chisel/)
>> Great write-up on several ways to use chisel

## jadx
> **Description**
>
> jadx is my go-to apk decompiler. It can be used in the command line with `jadx file.apk`, which dumps the java classes and other resources to a `file` directory. Alternatively, you can use jadx-gui to quickly view the decompile Java code.
>
> **Resources**
>
> [jadx on GitHub](https://github.com/skylot/jadx)
>> The GitHub Repository for jadx

## NetworkMiner
> **Description**
>
> NetworkMiner is a tool that can be used to dissect pcap/pcapng files and give you a lot of pertinent information in an incredibly easy to parse and quick manner. If you're looking for things like credentials in a pcap, this should be one of your first stops.
> 
> It is worth noting that the free version cannot use pcapng files. If you have a pcapng, you can usually convert it to a pcap with Wireshark.
>
> **Platforms**
> 
> * Windows
> * Linux (mono)
>
> **Resources**
> 
> [Project Page](https://www.netresec.com/?page=NetworkMiner)
>> This is the primary page for NetworkMiner

## Wireshark
> **Description**
>
> The de-facto standard for analyzing network traffic. Allows you to capture traffic on an interface (or import a pcap/pcapng of previously captured traffic) and displays information about every frame in that captured traffic.
>
> **Platforms**
>
> * Everything
>
> **Resources**
> [Project Page](https://www.wireshark.org) 
>> The main page for WireShark

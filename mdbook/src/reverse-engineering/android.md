# Android Reverse Engineering

## Build An App

> Description
>
> If you don't know how to engineer something, it's a lot harder to reverse engineer it. I recommend you start with the below resources to learn how to build your first app.
>
> Resources
>
> [Google's First App Tutorial](https://developer.android.com/training/basics/firstapp)
>> This is a good tutorial to walk you through the basics of creating an Android App.

## Tutorials

> Description
>
> There are many good resources out there that teach Android Reverse Engineering.
>
> Resources
>
> [Android App Reverse Engineering 101 (Maddie Stone)](https://maddiestone.github.io/AndroidAppRE/)
>> This is a great walkthrough with text, videos, and VM's to get you started with Reverse Engineering Android applications for static analysis.


## Tools

### Android Security Awesome

> Description
>
> Android Security Awesome is a GitHub Repo with a massive list of popular Android Reverse Engineering and PenTesting Tools.
>
> Resources
>
> [android-security-awesome](https://github.com/ashishb/android-security-awesome/)
>> Collection of resources to get you started with static and dynamic Android application analysis.

### Frida

> Description
>
> An amazing tool that lets you do all sorts of analysis on processes and hook (override) functions.
>
> Resources
>
> [HackTricks Frida Tutorial](https://book.hacktricks.xyz/mobile-apps-pentesting/android-app-pentesting/frida-tutorial)
>> This is probably the best tutorial I've found on Frida basics this far. Definitely worth checking out.
>
> [Android Hacking with FRIDA](https://joshspicer.com/android-frida-1)
>> Another very worthwhile guide for using Frida. This one goes through OWASP's `UnCrackable` APKs
